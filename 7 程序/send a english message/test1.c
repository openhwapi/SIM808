/************************************************************
	
	程序说明：
	功能：TEXT(GSM)模式下发送英文短消息，短消息直接显示在终端
	首先要确定模块已经注册到网络
	然后正确的硬件连接   P3.0-----STXD或者5VT   P3.1-----SRXD或者5VR   GND---GND（只要保证公地即可，没必要单独接一次）
	然后确认你单片机上的晶振，根据晶振修改自己的程序。
	推荐先将单片机与电脑相连，确定单片机发送的数据是正确的。如果发送的是乱码，请检查晶振与单片机的串口波特率。
	如果通过以上几条还解决不了问题，请看群共享文件 AN0004 。

*************************************************************/
#include <REG51.H>
#include <string.H>
#include <intrins.h>
#define uchar unsigned char
#define uint unsigned int
//以下是板子上LED的配置，把Px_x改成自己对应的脚。
//以下是你的51单片机的晶振大小
#define FOSC_110592M
//#define FOSC_12M
sbit P10=P1^0;
sbit P11=P1^1;
sbit P12=P1^2;
sbit P13=P1^3;
sbit P14=P1^4;
sbit P15=P1^5;
//以下是开机后发送到手机的内容，发送的号码在程序中修改。	
unsigned int  rec_data_len_uart=0;    //标记Buffer_Uart0接收数组
unsigned char idata Buffer_Uart0_Rec[25]={0};		 //Uart0中断接收数组
   
//注意，无论接收到信号还是发送完信号，都会进中断服务程序的
/*初始化程序（必须使用，否则无法收发），次程序将会使用定时器1*/
void SerialInti()//初始化程序（必须使用，否则无法收发）
{
	TMOD=0x20;//定时器1操作模式2:8位自动重载定时器

#ifdef FOSC_12M		   //在这里根据晶振大小设置不同的数值初始化串口
	TH1=0xf3;//装入初值，波特率2400
	TL1=0xf3;	
#else 	
	TH1=0xfd;//装入初值，波特率9600
	TL1=0xfd;
#endif //end of SOC_12M
	
	TR1=1;//打开定时器
	SM0=0;//设置串行通讯工作模式，（10为一部发送，波特率可变，由定时器1的溢出率控制）
	SM1=1;//(同上)在此模式下，定时器溢出一次就发送一个位的数据
	REN=1;//串行接收允许位（要先设置sm0sm1再开串行允许）
	EA=1;//开总中断
	ES=1;//开串行口中断	
}
unsigned char hand(unsigned char *data_source,unsigned char *ptr)
{
	if(strstr(data_source,ptr)!=NULL)
		return 1;
	else
		return 0;
}
void clear_rec_data()
{
	uchar i,temp_len;
	temp_len=strlen(Buffer_Uart0_Rec);
	if(temp_len>25)
	{
		temp_len=25;
	}
	for(i=0;i<temp_len;i++)
	{
		Buffer_Uart0_Rec[i]='\0';	
	}
	rec_data_len_uart=0;
}

/*串行通讯中断，收发完成将进入该中断*/
void Serial_interrupt() interrupt 4 
{
	unsigned char temp_rec_data_uart0;	
	temp_rec_data_uart0 = SBUF;//读取接收数据		
	RI=0;//接收中断信号清零，表示将继续接收			
	Buffer_Uart0_Rec[rec_data_len_uart]=temp_rec_data_uart0;	//接收数据
	rec_data_len_uart++;
	if(rec_data_len_uart>24)
	{
		rec_data_len_uart=0; 	//从头开始接收数据
	}	
}
void Uart1Send(uchar c)
{
	SBUF=c;
	while(!TI);//等待发送完成信号（TI=1）出现
	TI=0;	
}
//串行口连续发送char型数组，遇到终止号/0将停止
void Uart1Sends(uchar *str)
{
	while(*str!='\0')
	{
		SBUF=*str;
		while(!TI);//等待发送完成信号（TI=1）出现
		TI=0;
		str++;
	}
}
//延时函数大概是1s钟，不过延时大的话不准...
void DelaySec(int sec)
{
	uint i , j= 0;
	for(i=0; i<sec; i++)
	{
		for(j=0; j<65535; j++)
		{	
		}
	}
}
void main()
{
	uchar i = 0;
	SerialInti();
    DelaySec(10);//延时约15秒,此处延时，是为了让模块有足够的时间注册到网络，
    P14=1;		 //提示开始发送指令，开始发送指令时，务必确认模块上的LED 已经慢闪，即模块已经注册到网络
//----------------为什么是下面这些AT指令呢，请看群共享文件SIM900A重要的短信指令文件------------
    Uart1Sends("AT+CSCS=\"GSM\"\r\n");
    DelaySec(1);//延时大约3秒
    Uart1Sends("AT+CMGF=1\r\n");
    DelaySec(1);//延时3秒
	Uart1Sends("AT+CSCA?\r\n");
    DelaySec(1);//延时3秒
	Uart1Sends("AT+CSMP=17,167,0,240\r\n");
    DelaySec(1);//延时3秒
    Uart1Sends("AT+CMGS=\"18846920089\"\r\n");//此处修改为对方的电话号
    DelaySec(1);//延时3秒
    Uart1Sends("HELLO 123");//修改短信内容,短信内容可为英文和数字
    Uart1Send(0x1a);
    DelaySec(15);//延时20秒
    while(1);
}
